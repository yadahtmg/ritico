package com.ritico.interfaces;

import java.sql.Time;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

import com.ritico.domain.Booking;
import com.ritico.domain.Departure;
import com.ritico.domain.SchedulesLink;
import com.ritico.domain.TimeSchedules;
import com.ritico.domain.User;

public interface UserInt {
	
	public User saveUSer(User user);
	public Booking saveBookedTicket(Booking booking);
	public Departure saveDeparture(Departure user);
	public TimeSchedules saveTimeschedules(TimeSchedules timeschedule);
	public SchedulesLink saveScheduleLink(SchedulesLink schedulelink);
	void deleteUser(String users);
	void updateSeatUsingId(int numberofseats,String id);
	public User findbyname(String user);
	public User finduserbyid(String user);
	public Departure findbyDestination(String departure);
	public Departure findbyDepartureid(String departureid);
	public TimeSchedules findbyDepertHour(String hour);
	public TimeSchedules findSchedulesbyid(String id);
	public List<SchedulesLink> findSchedulesLinkbyid(Departure id);
	public SchedulesLink findSchedulesLinkbyid2(String id);
	//SchedulesLink findByDepartureeid(Departure timeschedules);
	Iterable<User> findAllUSers();
	Iterable<User> findAllUSersByid(String id);
	Iterable<Departure> findAllDeparture();
	Iterable<TimeSchedules> findAllTimeSchedules();
	Iterable<SchedulesLink> findAllSchedulesLink();
	Iterable<SchedulesLink> findAllSchedulesLinkWithid(String id);
	Iterable<Booking> findAllBooks();
	Iterable<Booking> findAllBooksWhereUserName(String username);
	/* Iterable<Departure> findAllInBothTablesJoined(); */
}
