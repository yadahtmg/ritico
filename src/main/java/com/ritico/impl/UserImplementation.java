package com.ritico.impl;

import java.sql.Time;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

import javax.print.attribute.standard.SheetCollate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ritico.domain.Booking;
import com.ritico.domain.Departure;
import com.ritico.domain.SchedulesLink;
import com.ritico.domain.TimeSchedules;
import com.ritico.domain.User;
import com.ritico.interfaces.UserInt;
import com.ritico.repository.BookingRepository;
import com.ritico.repository.DepartureRepository;
import com.ritico.repository.DeparturetimeRepository;
import com.ritico.repository.ScheduleLinkRepo;
import com.ritico.repository.UserRepository;

@Service
public class UserImplementation implements UserInt {
	@Autowired
	UserRepository userRepository;
	@Autowired
	DepartureRepository departureRepository;

	@Autowired
	DeparturetimeRepository departuretimeRepository;
	@Autowired
	ScheduleLinkRepo schedulesrepo;
	@Autowired
	BookingRepository bookingrepository;

	@Override
	public User saveUSer(User user) {
		return userRepository.save(user);
	}

	@Override
	public void deleteUser(String users) {
		// TODO Auto-generated method stub
		userRepository.deleteById(users);
	}

	@Override
	public User findbyname(String user) {
		// TODO Auto-generated method stub
		return userRepository.findByUsername(user);
	}

	@Override
	public Iterable<User> findAllUSers() {

		return userRepository.findAll();
	}

	@Override
	public Departure saveDeparture(Departure departure) {
		// TODO Auto-generated method stub
		return departureRepository.save(departure);
	}

	@Override
	public Departure findbyDestination(String departure) {
		// TODO Auto-generated method stub
		return departureRepository.findByDestinationname(departure);
	}

	@Override
	public User finduserbyid(String user) {

		return userRepository.findByIdnum(user);
	}

	@Override
	public TimeSchedules saveTimeschedules(TimeSchedules timeschedule) {
		// TODO Auto-generated method stub
		return departuretimeRepository.save(timeschedule);
	}

	@Override
	public TimeSchedules findbyDepertHour(String hour) {
		return departuretimeRepository.findByDeperthour(hour);
	}

	@Override
	public TimeSchedules findSchedulesbyid(String id) {
		// TODO Auto-generated method stub
		return departuretimeRepository.findByIdnum(id);
	}

	@Override
	public Departure findbyDepartureid(String departureid) {
		// TODO Auto-generated method stub
		return departureRepository.findByIdnum(departureid);
	}

	@Override
	public Iterable<Departure> findAllDeparture() {
		// TODO Auto-generated method stub
		return departureRepository.findAll();
	}

	@Override
	public Iterable<TimeSchedules> findAllTimeSchedules() {
		// TODO Auto-generated method stub
		return departuretimeRepository.findAll();

	}

	/*
	 * @Override public Iterable<Departure> findAllInBothTablesJoined() { // TODO
	 * Auto-generated method stub return departureRepository.findAllInBothTables();
	 * }
	 */

	@Override
	public SchedulesLink saveScheduleLink(SchedulesLink schedulelink) {

		return schedulesrepo.save(schedulelink);
	}

	@Override
	public Iterable<SchedulesLink> findAllSchedulesLink() {
		// TODO Auto-generated method stub
		return schedulesrepo.findAll();
	}

	@Override
	public Booking saveBookedTicket(Booking booking) {
		// TODO Auto-generated method stub
		return bookingrepository.save(booking);
	}

	@Override
	public Iterable<User> findAllUSersByid(String id) {
		// TODO Auto-generated method stub
		return userRepository.findAll();
	}

	@Override
	public List<SchedulesLink> findSchedulesLinkbyid(Departure id) {
		return schedulesrepo.findByDeparturee(id);
	}

	/*
	 * @Override public SchedulesLink findByDepartureeid(Departure timeschedules) {
	 * 
	 * return schedulesrepo.findByDeparturee(timeschedules); }
	 */
	@Override
	public Iterable<Booking> findAllBooks() {
		// TODO Auto-generated method stub
		return bookingrepository.findAll();
	}

	@Override
	public Iterable<SchedulesLink> findAllSchedulesLinkWithid(String id) {
		// TODO Auto-generated method stub
		return schedulesrepo.findAll();
	}

	@Override
	public SchedulesLink findSchedulesLinkbyid2(String id) {
		// TODO Auto-generated method stub
		return schedulesrepo.findByIdnum(id);
	}

	@Override
	public Iterable<Booking> findAllBooksWhereUserName(String username) {
		// TODO Auto-generated method stub
		return bookingrepository.findByScheduleslinkDepartureeUserUsername(username);
	}

	@Override
	public void updateSeatUsingId(int numberofseats,String id) {
		// TODO Auto-generated method stub
		departuretimeRepository.updaSeats(numberofseats,id);
	}

	/*
	 * @Override public SchedulesLink findByDepartureeid(Departure timeschedules) {
	 * // TODO Auto-generated method stub return null; }
	 */
}
