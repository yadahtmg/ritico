package com.ritico.impl;

public class EmailDuplicateException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String message;
	
     public EmailDuplicateException(String message) {
		// TODO Auto-generated constructor stub
    	this.message = message;
	}

    // Overrides Exception's getMessage()
    @Override
    public String getMessage(){
        return message;
    }
	

}
