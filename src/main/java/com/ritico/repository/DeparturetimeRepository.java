package com.ritico.repository;

import java.sql.Time;
import java.time.LocalTime;
import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ritico.domain.Departure;
import com.ritico.domain.TimeSchedules;


@Repository
public interface DeparturetimeRepository extends PagingAndSortingRepository<TimeSchedules, String> {
	TimeSchedules findByDeperthour(String timehour);
	TimeSchedules findByIdnum(String id);
	@Transactional
	@Modifying
	@Query("update TimeSchedules t set t.numberofseats = ?1 where t.idnum = ?2")
	void updaSeats(int numberofseats,String id);
	
	//Departure  findByDestinationname(String estinationname);
	
	/*
	 * User findByIdnum(String id);
	 * 
	 * User findByActive(User active);
	 * 
	 * Iterable<User>
	 * findByNewuserEqualsAndRolesEqualsOrRolesEqualsOrRolesEquals(String neww,
	 * String visitor, String admin, String farmer);
	 */
	// Iterable<User> findByRolesEquals(String visitor);

	/*
	 * Iterable<User> findByRolesEquals(String farmer);
	 * 
	 * long countByNewuserAndRolesEquals(String newuser, String roletype); long
	 * countByNewuser(String newuser);
	 */
	
	
	
//	@Modifying(clearAutomatically = true)
//	@Query("update User u set u.roles = FARMERR where u.username = :email")
//	void updateRoles(@Param("email") String email);

}
