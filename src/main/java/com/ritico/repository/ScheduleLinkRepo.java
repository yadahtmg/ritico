package com.ritico.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import com.ritico.domain.Departure;
import com.ritico.domain.SchedulesLink;
import com.ritico.domain.TimeSchedules;
import com.ritico.domain.User;

@Repository
public interface ScheduleLinkRepo extends PagingAndSortingRepository<SchedulesLink, String> {
	
  	//Iterable<SchedulesLink> findAll(String idnum);
  	

	 List<SchedulesLink>  findByDeparturee(Departure departuree);
	
	 
	 SchedulesLink findByIdnum(String idnum);
	 
	 SchedulesLink findByTimeschedulesIdnum(String timeschedules);
	 
	
	// Iterable<SchedulesLink> findaall(String id);
	/*
	 * 
	 * 
	 * Departure findByDestinationname(String destinationname); Departure
	 * findByIdnum(String idnum);
	 * 
	 * @Query("select t.deperthour, t.numberofseats,d.departuredate,d.destinationname,d.price from Departure d,TimeSchedules t where d.idnum=t.departure"
	 * ) Iterable<Departure> findAllInBothTables();
	 */
	/*
	 * User findByIdnum(String id);
	 * 
	 * User findByActive(User active);
	 * 
	 * Iterable<User>
	 * findByNewuserEqualsAndRolesEqualsOrRolesEqualsOrRolesEquals(String neww,
	 * String visitor, String admin, String farmer);
	 */
	// Iterable<User> findByRolesEquals(String visitor);

	/*
	 * Iterable<User> findByRolesEquals(String farmer);
	 * 
	 * long countByNewuserAndRolesEquals(String newuser, String roletype); long
	 * countByNewuser(String newuser);
	 */
	
	
	
//	@Modifying(clearAutomatically = true)
//	@Query("update User u set u.roles = FARMERR where u.username = :email")
//	void updateRoles(@Param("email") String email);

}
