package com.ritico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RiticoApplication {

	public static void main(String[] args) {
		SpringApplication.run(RiticoApplication.class, args);
	}

}
