package com.ritico.domain;


import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;





@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "destinationname")) 
public class Departure {
	@Column(unique = true)
	@Id
	private String idnum = UUID.randomUUID().toString();
	@NotBlank(message = "destination name is mandatory")
	private String destinationname;
	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date departuredate; 
	@NotNull
	@DecimalMax("5000.0") @DecimalMin("0.0") 
	private Double price;
	/*
	 * @NotBlank(message = "contact is mandatory") private String contactperson;
	 */
	@ManyToOne
	private User user;
	@OneToMany(mappedBy = "departuree", cascade = CascadeType.ALL)
	private List<SchedulesLink> scheduleslink;
	public String getIdnum() {
		return idnum;
	}
	public void setIdnum(String idnum) {
		this.idnum = idnum;
	}
	public Date getDeparturedate() {
		return departuredate;
	}
	public void setDeparturedate(Date departuredate) {
		this.departuredate = departuredate;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}

	/*
	 * public String getContactperson() { return contactperson; } public void
	 * setContactperson(String contactperson) { this.contactperson = contactperson;
	 * }
	 */
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	public List<SchedulesLink> getScheduleslink() {
		return scheduleslink;
	}
	public void setScheduleslink(List<SchedulesLink> scheduleslink) {
		this.scheduleslink = scheduleslink;
	}
	public String getDestinationname() {
		return destinationname;
	}
	public void setDestinationname(String destinationname) {
		this.destinationname = destinationname;
	}
	
   
	 @Override
	public String toString() {
		
		 return "[" + getPrice()
          + getDestinationname() + getDeparturedate()+"]";
	}
	
	

}
