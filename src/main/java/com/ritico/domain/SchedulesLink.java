package com.ritico.domain;

import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.Valid;
@Entity
public class SchedulesLink {
	@Id
	private String idnum = UUID.randomUUID().toString();
	@Valid
	@ManyToOne
	private Departure departuree;
	@Valid
	@ManyToOne
	private TimeSchedules timeschedules;
	@OneToMany(mappedBy = "scheduleslink", cascade = CascadeType.ALL)
	private List<Booking> bookingaseat;
	
	public String getIdnum() {
		return idnum;
	}
	public void setIdnum(String idnum) {
		this.idnum = idnum;
	}
	public Departure getDeparturee() {
		return departuree;
	}
	public void setDeparture(Departure departuree) {
		this.departuree = departuree;
	}
	public TimeSchedules getTimeschedules() {
		return timeschedules;
	}
	public void setTimeschedules(TimeSchedules timeschedules) {
		this.timeschedules = timeschedules;
	}
	
	public List<Booking> getBookingaseat() {
		return bookingaseat;
	}
	public void setBookingaseat(List<Booking> bookingaseat) {
		this.bookingaseat = bookingaseat;
	}
	public void setDeparturee(Departure departuree) {
		this.departuree = departuree;
	}

	
	
	
}
