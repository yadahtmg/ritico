package com.ritico.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;




@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "username")) 
public class User {
	@Column(unique = true)
	@Id
	private String idnum = UUID.randomUUID().toString();
	private Date registrationdate= new Date();
	@NotBlank(message = "Firstname is mandatory")
	private String userfirstname;
	@NotBlank(message = "LastName is mandatory")
	private String userlastname;
	@NotBlank(message = "phone contact  is mandatory")
	@Pattern(regexp = "^[0-9]{10}", message = "length must be 10 and must be numbers only ")
	private String contactnumber;
	@Transient
	private String newuser;
	@NotBlank(message = "Gender is mandatory")
	private String gender;
	@NotBlank(message = "Email is mandatory")
	private String username;
	@NotBlank(message = "passowrd is mandatory")
	private String passwordd;
	@NotBlank(message = "UserType is mandatory")
	private String roles = "";
	private String active = "BLOCK";
	@OneToMany(mappedBy = "user",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	private List<Departure> departures;
	  
	  public User(String username, String passwordd, String roles) { this.username
	  = username; this.passwordd = passwordd; this.roles = roles; this.active =
	  "ACTIVE"; }
	  
	  public User() {
	  
	  }
	 
	public String getIdnum() {
		return idnum;
	}

	public void setIdnum(String idnum) {
		this.idnum = idnum;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPasswordd() {
		return passwordd;
	}

	public void setPasswordd(String passwordd) {
		this.passwordd = passwordd;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	
	  public String getRoles() { return roles; }
	  
	  public void setRoles(String roles) { this.roles = roles; }
	  
	  public List<String> getRoleList() { if (this.roles.length() > 0) { return
	  Arrays.asList(this.roles.split(",")); } return new ArrayList<String>(); }
	 
	public String getUserfirstname() {
		return userfirstname;
	}

	public void setUserfirstname(String userfirstname) {
		this.userfirstname = userfirstname;
	}

	public String getUserlastname() {
		return userlastname;
	}

	public void setUserlastname(String userlastname) {
		this.userlastname = userlastname;
	}

	public String getContactnumber() {
		return contactnumber;
	}

	public void setContactnumber(String contactnumber) {
		this.contactnumber = contactnumber;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	

	 
	  
	
//	public String getRoles() {
//		return roles;
//	}
//
//	public void setRoles(String roles) {
//		this.roles = roles;
//	}
	
	
	public List<Departure> getDepartures() {
		return departures;
	}

	public void setDepartures(List<Departure> departures) {
		this.departures = departures;
	}

	public Date getRegistrationdate() {
		return registrationdate;
	}

	public void setRegistrationdate(Date registrationdate) {
		this.registrationdate = registrationdate;
	}
    
	
	
	public String getNewuser() {
		return newuser;
	}

	public void setNewuser(String newuser) {
		this.newuser = newuser;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return username;
	}

//public List<String> getPermissionss(){
//	if(this.permissions.length() > 0) {
//		return Arrays.asList(this.permissions.split(","))  ;
//	}
//	return new ArrayList<String>();
//}

}
