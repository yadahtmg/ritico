package com.ritico.domain;

public class HandlesDepartureHoursList {
private String idnum;
private String deperthours;
public String getIdnum() {
	return idnum;
}
public void setIdnum(String idnum) {
	this.idnum = idnum;
}
public String getDeperthours() {
	return deperthours;
}
public void setDeperthours(String deperthours) {
	this.deperthours = deperthours;
}

public HandlesDepartureHoursList(String idnum, String deperthours) {
	super();
	this.idnum = idnum;
	this.deperthours = deperthours;
}


}
