package com.ritico.domain;

import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Entity
public class Booking {
	@Column(unique = true)
	@Id
	private String idnum = UUID.randomUUID().toString();
	@NotBlank(message = "Client names can't be empty")
	private String clientnames;
	@NotBlank(message = "phone contact  is mandatory")
	@Pattern(regexp = "^[0-9]{10}", message = "length must be 10 and must be numbers only ")
	private String contantactperson;
	private String useremail;
	@Valid
	@ManyToOne
	private  SchedulesLink scheduleslink;
	private int numberofseats;
	public String getClientnames() {
		return clientnames;
	}
	public void setClientnames(String clientnames) {
		this.clientnames = clientnames;
	}
	public String getContantactperson() {
		return contantactperson;
	}
	public void setContantactperson(String contantactperson) {
		this.contantactperson = contantactperson;
	}
	
	
	public SchedulesLink getScheduleslink() {
		return scheduleslink;
	}
	public void setScheduleslink(SchedulesLink scheduleslink) {
		this.scheduleslink = scheduleslink;
	}
	public String getIdnum() {
		return idnum;
	}
	public void setIdnum(String idnum) {
		this.idnum = idnum;
	}
	public String getUseremail() {
		return useremail;
	}
	public void setUseremail(String useremail) {
		this.useremail = useremail;
	}
	public int getNumberofseats() {
		return numberofseats;
	}
	public void setNumberofseats(int numberofseats) {
		this.numberofseats = numberofseats;
	}
	
	
	
	
}
