package com.ritico.domain;

import java.sql.Time;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "deperthour"))
public class TimeSchedules {
	@Column(unique = true)
	@Id
	private String idnum = UUID.randomUUID().toString();

	//@Temporal(TemporalType.TIME)
	//@DateTimeFormat(pattern = "hh:mm a")
	private String deperthour;
	@NotNull
	@DecimalMin("0") @DecimalMax("45") 
	private int numberofseats;
	@OneToMany(mappedBy = "timeschedules", cascade = CascadeType.ALL)
	private List<SchedulesLink> scheduleslink;

	public String getIdnum() {
		return idnum;
	}

	public void setIdnum(String idnum) {
		this.idnum = idnum;
	}

	public String getDeperthour() {
		return deperthour;
	}

	public void setDeperthour(String deperthour) {
		this.deperthour = deperthour;
	}

	public int getNumberofseats() {
		return numberofseats;
	}

	public void setNumberofseats(int numberofseats) {
		this.numberofseats = numberofseats;
	}


  
	 
	 public List<SchedulesLink> getScheduleslink() {
		return scheduleslink;
	}

	public void setScheduleslink(List<SchedulesLink> scheduleslink) {
		this.scheduleslink = scheduleslink;
	}

	@Override
	public String toString() {
		
		 return "[" + getDeperthour()
         + getNumberofseats()+ "]";
	}
	
}
