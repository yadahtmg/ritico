
package com.ritico.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.ritico.domain.Departure;
import com.ritico.domain.HandlesDepartureHoursList;
import com.ritico.domain.SchedulesLink;
import com.ritico.domain.TimeSchedules;
import com.ritico.interfaces.UserInt;

@RestController
public class RequestApi {

	@Autowired
	UserInt userint;

	@RequestMapping(value = "/loadAllDeparture", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> bookingPagess(@RequestParam String destinationid) { // User user =
		// userint.finduserbyid(destinationid);
		Departure departure = userint.findbyDepartureid(destinationid);

		List<SchedulesLink> scheduler = userint.findSchedulesLinkbyid(departure);
		List<HandlesDepartureHoursList> handlesDepartureHoursList = new ArrayList<HandlesDepartureHoursList>();

		String time = "";
		String id = "";
		for (SchedulesLink bk : scheduler) {
			time = bk.getTimeschedules().getDeperthour();
			id = bk.getIdnum();
			handlesDepartureHoursList.add(new HandlesDepartureHoursList(id, time));
		}
		
		System.out.println("--------------\n" + time + "okkkkkkkkkk");

		Map<String, List<HandlesDepartureHoursList>> map = new HashMap<>();

		map.put("listofhours", handlesDepartureHoursList);
		// map.put("idnum",scheduler.getIdnum());

		// map.put("deperthours",scheduleslink.getTimeschedules().getDeperthour());
		return new ResponseEntity<>(map, HttpStatus.OK);

	}

}
