package com.ritico.controller;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.management.Notification;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.GsonFactoryBean;
//import org.springframework.mail.MailException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.ritico.domain.Booking;
import com.ritico.domain.Departure;
import com.ritico.domain.SchedulesLink;
import com.ritico.domain.TimeSchedules;
import com.ritico.domain.User;
import com.ritico.impl.EmailDuplicateException;
import com.ritico.interfaces.UserInt;
import com.sun.org.apache.bcel.internal.generic.NEW;

import net.bytebuddy.asm.Advice.Return;

@Controller("")
public class Ritico {
	@Autowired
	private UserInt userint;

	@GetMapping("/")
	public String homePage() {
		return "index.html";
	}

	@GetMapping("/about")
	public String aboutPage() {
		return "about.html";
	}

	@GetMapping("/bookingseats")
	public String bookingPage(Model model) {

		model.addAttribute("departure_view4", userint.findAllDeparture());
		model.addAttribute("alldeperturetimes", userint.findAllTimeSchedules());
		model.addAttribute("timeschedulesall", userint.findAllSchedulesLink());
		model.addAttribute("bookinghere", new Booking());

		return "bookingatravel/booking.html";
	}

	@PostMapping("/bookingseats")
	public String bookingTravel(@Valid @ModelAttribute("bookinghere") Booking bookinghere, Errors binding,
			@ModelAttribute("seats") String seats, RedirectAttributes redirect) {
		if (binding.hasErrors()) {

			return "bookingatravel/booking.html";

		}
		String id = bookinghere.getScheduleslink().getIdnum();

		SchedulesLink scheduler = userint.findSchedulesLinkbyid2(id);
		String timescheduleId = scheduler.getTimeschedules().getIdnum();
		int numberofseats = scheduler.getTimeschedules().getNumberofseats();
		int seatneedbyaclient = bookinghere.getNumberofseats();
		int results = (numberofseats - seatneedbyaclient);
		System.out.println(numberofseats);
		System.out.println(seatneedbyaclient);
        System.out.println(results);
		try {
            if(numberofseats<seatneedbyaclient) {
            	redirect.addFlashAttribute("msg","Sorry at this"+"  "+scheduler.getTimeschedules().getDeperthour()+" we have  " +numberofseats+ " seats only kindly choose another hour or wait tomorrow");
            }else {
			bookinghere.setScheduleslink(scheduler);
			userint.saveBookedTicket(bookinghere);
			userint.updateSeatUsingId(results,timescheduleId);
			redirect.addFlashAttribute("msg",
					"Request sent successfully!! please pay before 30 min on Momo 07867636323 and remember to bring the message that shows u've payed/fail to this your request will be cancelled ");
            }} catch (Exception e) {
			e.printStackTrace();
		}

		return "redirect:/bookingseats";
	}

	// end of booking a travel section

	@GetMapping("/departurehome")
	public String departurePage(Model model, Departure depart) {
		model.addAttribute("departure_view2", userint.findAllDeparture());
		model.addAttribute("departure_view3", userint.findAllSchedulesLink());
		// model.addAttribute("departure_view3", userint.findAllInBothTablesJoined());
		// Iterable<TimeSchedules> time = userint.findAllInBothTablesJoined();
		// System.out.println(time);
		return "departure.html";
	}

	@GetMapping("/admin2")
	public String admin2Page() {
		return "admin/admin.html";
	}

	@GetMapping("/departuretimeview")
	public String departureTimeViewePage(Model model, Principal principal) {
		String user = principal.getName();
		User existing = userint.findbyname(user);
		model.addAttribute("loggedinuser",
				existing.getUserfirstname().toUpperCase().charAt(0) + "."
						+ existing.getUserlastname().substring(0, 1).toUpperCase()
						+ existing.getUserlastname().substring(1).toLowerCase());

		model.addAttribute("departuretimeview", userint.findAllSchedulesLink());

		return "administrationpages/departurehour/departurehourview.html";
	}

	@GetMapping("/departuretime")
	public String departureSchedurePage(Model model, Principal principal) {
		String user = principal.getName();
		User existing = userint.findbyname(user);
		model.addAttribute("loggedinuser",
				existing.getUserfirstname().toUpperCase().charAt(0) + "."
						+ existing.getUserlastname().substring(0, 1).toUpperCase()
						+ existing.getUserlastname().substring(1).toLowerCase());

		model.addAttribute("timeschedules", new TimeSchedules());
		model.addAttribute("alldeparture", userint.findAllDeparture());
		return "administrationpages/departurehour/departurehour.html";
	}

	@GetMapping("/departureview")
	public String departureview(Model model, Principal principal) {
		String user = principal.getName();
		User existing = userint.findbyname(user);
		model.addAttribute("loggedinuser",
				existing.getUserfirstname().toUpperCase().charAt(0) + "."
						+ existing.getUserlastname().substring(0, 1).toUpperCase()
						+ existing.getUserlastname().substring(1).toLowerCase());
		model.addAttribute("departure_view", userint.findAllDeparture());
		return "administrationpages/departure/departureview.html";
	}

	@GetMapping("/contact")
	public String contactPage() {
		return "contact.html";
	}

	@GetMapping("/departure")
	public String departurePage(Model model, Principal principal) {
		String user = principal.getName();
		User existing = userint.findbyname(user);
		model.addAttribute("loggedinuser",
				existing.getUserfirstname().toUpperCase().charAt(0) + "."
						+ existing.getUserlastname().substring(0, 1).toUpperCase()
						+ existing.getUserlastname().substring(1).toLowerCase());

		model.addAttribute("depart", new Departure());
		model.addAttribute("allusers", userint.findAllUSers());
		return "administrationpages/departure/departure.html";
	}

	@GetMapping("/register")
	public String registrationPage(Model model) {
		model.addAttribute("myobj", new User());

		return "register/register";
	}

	@GetMapping("/admin")
	public String adminPage(Model model, Principal principal) {
		model.addAttribute("viewalluser", userint.findAllUSers());
		String user = principal.getName();
		User existing = userint.findbyname(user);
		model.addAttribute("loggedinuser",
				existing.getUserfirstname().toUpperCase().charAt(0) + "."
						+ existing.getUserlastname().substring(0, 1).toUpperCase()
						+ existing.getUserlastname().substring(1).toLowerCase());

		System.out.print(user);
		return "administrationpages/index";
	}

	@GetMapping("/normaluser")
	public String userPage(Model model, Principal principal) {
		String user = principal.getName();
		User existing = userint.findbyname(user);
		model.addAttribute("loggedinuser",
				existing.getUserfirstname().toUpperCase().charAt(0) + "."
						+ existing.getUserlastname().substring(0, 1).toUpperCase()
						+ existing.getUserlastname().substring(1).toLowerCase());
		model.addAttribute("viewAllClientRequest", userint.findAllBooksWhereUserName(user));
		return "normaluser/normaluser.html";
	}

	@GetMapping("/login")
	public String loginPage() {
		return "login/login.html";
	}

	@PostMapping("/departuretime")
	public String saveDeparturetime(@Valid @ModelAttribute("timeschedules") TimeSchedules timeschedules,
			BindingResult bind, @ModelAttribute("hourid") String hourid, @ModelAttribute("hours") String hours,
			RedirectAttributes redirect) {
		if (bind.hasErrors()) {

			return "administrationpages/departurehour/departurehour.html";

		}

		try {

			SchedulesLink sch = new SchedulesLink();
			Departure depart = userint.findbyDepartureid(hourid);
			sch.setDeparture(depart);
			sch.setTimeschedules(timeschedules);
			userint.saveTimeschedules(timeschedules);
			userint.saveScheduleLink(sch);
			redirect.addFlashAttribute("msg", "departure saved successfully ");
		} catch (Exception e) {
			TimeSchedules existing = userint.findbyDepertHour(timeschedules.getDeperthour());
			EmailDuplicateException duplicateemail = new EmailDuplicateException(
					"Hours Already registered" + "  " + existing);
			redirect.addFlashAttribute("msg", duplicateemail.getMessage());
		}

		return "redirect:/departuretime";
	}

	@PostMapping("/departure")
	public String saveDepartures(@Valid @ModelAttribute("depart") Departure depart, BindingResult bind,
			@ModelAttribute("userid") String userid, RedirectAttributes redirect) {
		if (bind.hasErrors()) {

			return "administrationpages/departure/departure.html";

		}

		try {

			User user = userint.finduserbyid(userid);
			depart.setUser(user);
			userint.saveDeparture(depart);
			redirect.addFlashAttribute("msg", "departure saved successfully ");
		} catch (Exception e) {
			Departure existing = userint.findbyDestination(depart.getDestinationname());
			EmailDuplicateException duplicateemail = new EmailDuplicateException(
					"Departure Already registered" + "  " + existing.getDestinationname());
			redirect.addFlashAttribute("msg", duplicateemail.getMessage());
		}

		return "redirect:/departure";
	}

	@PostMapping("/register")
	public String saveUser(@Valid @ModelAttribute("myobj") User myobj, BindingResult bind,
			RedirectAttributes redirect) {
		if (bind.hasErrors()) {

			return "register/register.html";

		}

		/*
		 * User existing = userint.findbyname(myobj.getUsername()); if (existing !=
		 * null) { bind.rejectValue("username", null,
		 * "There is already an account registered with that email"); }
		 */

		/*
		 * try { myobj.setUsername(myobj.getUsername());
		 * 
		 * notification.sendNotification(myobj); } catch (MailException e) {
		 * 
		 * logger.info("message errore for email here" + e.getMessage()); }
		 */

		BCryptPasswordEncoder ki = new BCryptPasswordEncoder();
		myobj.setPasswordd(ki.encode(myobj.getPasswordd()));
		// firmServe.saveAdmin(user);
		/*
		 * User existing = userint.findbyname(myobj.getUsername()); if (existing !=
		 * null) { redirect.addFlashAttribute(
		 * "msg","there is already a user registered with such email"+" "+existing); }
		 */

		try {

			userint.saveUSer(myobj);
			redirect.addFlashAttribute("msg", "saved successfully wait for admin approval!!! ");
		} catch (Exception e) {
			User existing = userint.findbyname(myobj.getUsername());
			EmailDuplicateException duplicateemail = new EmailDuplicateException(
					"there is already  registered with This email" + "  " + existing);
			redirect.addFlashAttribute("msg", duplicateemail.getMessage());
		}

		return "redirect:/register";
	}

	@GetMapping("/deleteusr")
	public String deleteUsers(@RequestParam("myid") String id, RedirectAttributes redire) {
		userint.deleteUser(id);
		redire.addFlashAttribute("msg", "record deleted successfully");
		return "redirect:/admin";

	}

	@GetMapping("/update")
	public String updateUsers(@RequestParam("myidd") String id, User user, RedirectAttributes redire) {

		User usage = new User();
		usage = userint.finduserbyid(id);

		if (usage.getActive().equals("BLOCK")) {
			usage.setActive("ACTIVE");
			usage.setNewuser("");

			/*
			 * try { usage.setUsername(usage.getUsername()); notificate.sendMyEmail(usage);
			 * } catch (MailException e) {
			 * 
			 * logger.info("message errore for email here" + e.getMessage()); }
			 */
		} else {
			usage.setActive("BLOCK");

		}

		userint.saveUSer(usage);
		redire.addFlashAttribute("msg", "record updated successfully");
		return "redirect:/admin";

	}

	@GetMapping("/updateuser{stid}")
	public String updateStudent(@RequestParam("stid") String idnum, Model model) {
		model.addAttribute("myobj2", userint.finduserbyid(idnum));
		return "administrationpages/updateuser";
	}

	@GetMapping("/updateview")
	public String updat(Model model) {
		model.addAttribute("myobj2", new User());
		return "administrationpages/updateuser";
	}

	@PostMapping("/updateview")
	public String saveUpdatedUser(@Valid @ModelAttribute("myobj2") User myobj, BindingResult bind,
			RedirectAttributes redirect) {
		if (bind.hasErrors()) {

			return "administrationpages/updateuser";

		}

		/*
		 * User existing = userint.findbyname(myobj.getUsername()); if (existing !=
		 * null) { bind.rejectValue("username", null,
		 * "There is already an account registered with that email"); }
		 * 
		 * 
		 * 
		 * try { myobj.setUsername(myobj.getUsername());
		 * 
		 * notification.sendNotification(myobj); } catch (MailException e) {
		 * 
		 * logger.info("message errore for email here" + e.getMessage()); }
		 */

		BCryptPasswordEncoder ki = new BCryptPasswordEncoder();
		myobj.setPasswordd(ki.encode(myobj.getPasswordd())); //
		// firmServe.saveAdmin(user);

		/*
		 * User existing = userint.findbyname(myobj.getUsername()); if (existing !=
		 * null) { redirect.addFlashAttribute(
		 * "msg","there is already a user registered with such email"+" "+existing); }
		 */

		try {

			userint.saveUSer(myobj);
			redirect.addFlashAttribute("msg", "saved successfully wait for admin approval!!! ");
		} catch (Exception e) {
			User existing = userint.findbyname(myobj.getUsername());
			EmailDuplicateException duplicateemail = new EmailDuplicateException(
					"there is already  registered with This email" + "  " + existing);
			redirect.addFlashAttribute("msg", duplicateemail.getMessage());
		}

		return "redirect:/updateview";
	}

}
